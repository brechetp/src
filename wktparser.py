#!/home/user/.local/src/.venv/bin/python3

from bs4 import BeautifulSoup
from urllib import request
import argparse
import pandas as pd  # for tsv files
import subprocess
import tempfile

def main(args):
    filedata = pd.read_csv(args.file, header=0,  sep='\t')
    if 'meaning' not in filedata.columns:
        filedata['meaning'] = ''
    N = filedata.index.size  # number of words in the file

    filedata.index = pd.Index(range(1, N + 1))
    word = None

    if args.word is not None:
        word = args.word
        isin = filedata[filedata['word'] == word]  # true where the word lies
        if not isin:
            filedata.loc[N + 1]['word'] = word
    else:
        if args.all:
            filedata = process_all(filedata)
        else:
            word = filedata.iloc[-1]['word']
        # widx = filedata.size

    if word is not None:
        filedata = add_word(filedata, word)
    filedata.to_csv(args.file, sep='\t', index=False)


def process_all(filedata):
    """Add all the missing meanings"""

    mask = filedata['meaning'].isnull()  # the missing meanings
    for word in filedata[mask]['word']:
        filedata = add_word(filedata, word)
    return filedata


def add_word(filedata, word):
    """Add a word to the filedata"""

    widx = filedata[filedata['word'] == word].index[0]
    strval = query_word(word)

    filedata.loc[widx, 'meaning'] = strval

    return filedata


def query_word(word):

    tmpfile = tempfile.NamedTemporaryFile()

    url = f"https://de.wiktionary.org/wiki/{word}"

    subprocess.run(["wget", "-O", tmpfile.name, f"{url}"])
    # fname = "./Pasta"

    # with open(tmpfile.file, 'r') as _f:
    html = tmpfile.read()
    tmpfile.close()

    soup = BeautifulSoup(html, 'html.parser')

    if soup.body:
        semantik = soup.body.find(
            'p',
            {'title': 'Sinn und Bezeichnetes (Semantik)'})

        if semantik:
            lst = semantik.find_next('dl')  # list of all meanings

            textlst = [m.get_text() for m in lst.find_all('dd')]
            strval = '; '.join(textlst)
            return strval
    return ''


if __name__ == "__main__":

    parser = argparse.ArgumentParser(prog="WikiParser",
                                     description="Parser to get meaning of words for Wiktionary"
                                     )
    parser.add_argument("--word", '-w', help="the word to lookup")
    parser.add_argument("--file", "-f", default="/home/user/Documents/gathered.tsv", help="tsv file to output the meanings")
    parser.add_argument("--all", "-a", action="store_true", help="process all the word in the file that don't have a meaning yet")
    parser.add_argument("--verbose", "-v", action="store_true", help="verbose mode")

    args = parser.parse_args()
    main(args)

